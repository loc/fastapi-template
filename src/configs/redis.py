from redis import Redis
from .config import get_config

app_config = get_config()

config = {
    'encoding': 'utf-8',
    'encoding_errors': 'strict',
    'decode_responses': True,
}

redis = Redis(
    host=app_config.APP_REDIS_HOST,
    port=app_config.APP_REDIS_PORT,
    db=app_config.APP_REDIS_DB,
    **config
)


def check_redis_connection():
    redis.ping()
