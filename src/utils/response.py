from pydantic import BaseModel
from pydantic.generics import GenericModel
from typing import Generic, Optional, TypeVar

DataT = TypeVar('DataT')


class ErrorResponse(BaseModel):
    code: int
    message: Optional[DataT]


class ResponseModel(GenericModel, Generic[DataT]):

    data: Optional[DataT]
    error: Optional[ErrorResponse] = None

