from fastapi import APIRouter
from typing import List
from pydantic import BaseModel
from src.configs import redis
from src.models import LichPhatSong
from src.utils import ResponseModel


router = APIRouter(prefix='/misc')


@router.get('/health-check')
def health_check():
    return {
        'code': 1,
        'message': 'OK'
    }


@router.get('/set-redis')
def test_set_redis():
    redis.set('key_a', 'key_a_value')

    return {
        'OK'
    }


class ExampleResponse(BaseModel):
    channel_id: str
    channel_name: str


@router.get('/example-get-data-from-db', response_model=ResponseModel[List[ExampleResponse]])
def ex_get_data_from_db():
    data = LichPhatSong.objects.raw({}).limit(1).values()

    return {
        'data': list(data)
    }
